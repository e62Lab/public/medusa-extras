# CAD Files Import

IGES and STEP files can be imported into Medusa if they only contain NURBS
objects. Two functions are provided: `NURBSShape<Vec3d, Vec2d>
translate_IGES(const std::string &file_name, const int force_c = 0, const bool
skip = false)` and `NURBSShape<Vec3d, Vec2d> translate_STEP(const std::string
&file_name, const bool skip = false)`, see documentation in source.

See also the [corresponding entry on Medusa wiki](https://e6.ijs.si/medusa/wiki/index.php/Computer-aided_design_-_Importing_IGES_and_STEP_files).

## Working examples

- [Tiger](examples/tiger.step)
- [Dolphin](examples/dolphin.stp)
- [Lizard](examples/lizard.step)
- [Human](examples/human_man.iges) - very hard to fill the interior
- [Hand](examples/hand.igs) - skip should be set to true
- [Gear](examples/gear.stp) - skip should be set to true, top and bottom should
  be filled by hand
  
Usage example is provided in [discretize_tiger.cpp](discretize_tiger.cpp) with
the corresponding MATLAB plot script [plot_tiger.m](plot_tiger.m).

## Dependencies

- [OpenCASCADE](https://dev.opencascade.org/doc/overview/html/)
