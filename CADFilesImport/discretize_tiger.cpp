#include <medusa/Medusa_fwd.hpp>
#include "cad_files_input.hpp"

using namespace mm;

int main() {
    NURBSShape<Vec3d, Vec2d> shape = translate_STEP("examples/tiger.step");
    double h = 2;

    // Oversample the domain.
    DomainDiscretization<Vec3d> oversampled_domain = shape.discretizeBoundaryWithStep(h / 5);

    for (int i = 0; i < oversampled_domain.size(); i++) {
        oversampled_domain.normal(i) *= -1;
    }

    // Construct the contains function.
    KDTree<Vec3d> contains_tree;
    oversampled_domain.makeDiscreteContainsStructure(contains_tree);
    auto contains_function = [&] (const Vec3d p) {
        return oversampled_domain.discreteContains(p, contains_tree);
    };

    // Fill the boundary normally.
    DomainDiscretization<Vec3d> domain = shape.discretizeBoundaryWithStep(h);

    // Fill the interior with the contains function.
    KDTreeMutable<Vec3d> tree;
    GeneralFill<Vec3d> gf;
    gf(domain, h, tree, contains_function);

    // Write the solution into file.
    std::ofstream out_file("sampled_tiger_data.m");
    out_file << "positions = " << domain.positions() << ";" << std::endl;
    out_file.close();

    return 0;
}
