clear
close all
set(groot,'defaulttextinterpreter','latex');
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

run sampled_tiger_data.m

scatter3(positions(1, :), positions(2, :), positions(3, :), 5, [0, 0, 0], 'o', 'filled')
axis('equal')
xlabel('$x$')
ylabel('$y$')
ylabel('$z$')
view(3)
