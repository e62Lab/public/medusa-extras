import numpy as np
from h5py import *
import xml.etree.cElementTree as ET
import sys

if len(sys.argv) < 3:
    print('Too few arguments. 1st additional argument is the .h5 file, the second the desired name for the output.')
    exit()

if len(sys.argv) > 3:
    print('Too many arguments. 1st additional argument is the .h5 file, the second the desired name for the output.')
    exit()

def allkeys(obj):
    # "Recursively find all keys in an h5py.Group."
    keys = (obj.name,)
    if isinstance(obj, Group):
        for key, value in obj.items():
            if isinstance(value, Group):
                keys = keys + allkeys(value)
            else:
                keys = keys + (value.name,)
    return keys

def DataItem(parent, item, key):
    try:
        ET.SubElement(parent, 'DataItem', attrib={'Dimensions': str(len(item[key])) + ' ' + str(len(item[key][0])), 'Format': 'HDF'}).text = item.filename + ':' + key
    except:
        ET.SubElement(parent, 'DataItem', attrib={'Dimensions': str(len(item[key])) + ' ' + str(1), 'Format': 'HDF'}).text = item.filename + ':' + key
    
def Attribute(parent, item, key):
    type = 'Scalar'
    try:
        dims = len(item[key][0])
        if dims > 1:
            type = 'Vector'
        if dims > 3:
            type = 'Tensor'
    except:
        pass
    this = ET.SubElement(parent, 'Attribute', attrib={'AttributeType': type, 'Center': 'Node', 'Name': key})
    DataItem(this, item, key)

def WriteXDMF(item):
    xdmf = ET.Element('Xdmf', attrib={'Version': "2.2"})
    domain = ET.SubElement(xdmf, "Domain")
    grid = ET.SubElement(domain, 'Grid', attrib={'Name': "points", 'GridType': 'Uniform'})
    ET.SubElement(grid, 'Topology', attrib={'TopologyType': "Polyvertex", 'NumberOfElements': str(len(item['/domain/pos']))})
    types = 'XYZ'
    geometry = ET.SubElement(grid, 'Geometry', attrib={'Type': types[:len(item['/domain/pos'][0])]})
    DataItem(geometry, item, '/domain/pos')

    for key in allkeys(item):
        if key != '/domain' and key != '/':
            Attribute(grid, item, key)

    tree = ET.ElementTree(xdmf)
    ET.indent(tree, space='\t', level=0)

    tree.write(item.filename[:-3] + '.xdmf', encoding='utf-8')

fname = str(sys.argv[1])
data = File(fname, 'r')

ofname = str(sys.argv[2]) + '.h5'
f = File(ofname, 'a')
try:
    for key in allkeys(data):
        theThing = data[key]
        if isinstance(theThing, Dataset):
            try:
                if theThing.ndim > 1:
                    temp = np.transpose(theThing)
                    dset = f.create_dataset(key, (len(temp), len(temp[0])), dtype=data[key].dtype, data=temp)
                else:
                    temp = theThing
                    dset = f.create_dataset(key, (len(temp), ), dtype=data[key].dtype, data=temp)
            except:
                pass
except:
    print('I crashed, don\'t blame Filip.')
    f.close()
    exit()

data.close()
WriteXDMF(f)
f.close()