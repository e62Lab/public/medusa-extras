# Spline shape for 2D surface reconstruction

A spline is fitted to the boundary nodes and thus a continuous surface can reconstructed.

## More about the algorithm

A [scientific paper](https://www.researchgate.net/publication/353116869_Discretized_boundary_surface_reconstruction)
about the algorithm has been written.

## Examples

![dendrite](screenshots/dendrite.png)