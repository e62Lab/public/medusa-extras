//
// Created by mitja on 10/01/2022.
//

#ifndef MEDUSA_EXTRAS_CLOVER_SHAPE_2D_H
#define MEDUSA_EXTRAS_CLOVER_SHAPE_2D_H

#include <medusa/Medusa.hpp>
#include <cmath>

inline double clover_polar(double phi) {
    return 1.5 - mm::ipow<3>(std::cos(3 * (phi - mm::PI / 6)));
}

inline double clover_polar_der(double phi) {
    return -9 * mm::ipow<2>(std::sin(3 * phi)) * std::cos(3 * phi);
}

inline mm::Vec2d clover_param(const mm::Vec1d &phi) {
    double r = clover_polar(phi[0]);
    return mm::Vec2d(
            r * std::cos(phi[0]),
            r * std::sin(phi[0]));
}

inline mm::Vec2d clover_param_jac(const mm::Vec1d &phi) {
    double r = clover_polar(phi[0]);
    double rd = clover_polar_der(phi[0]);
    return mm::Vec2d(
            rd * std::cos(phi[0]) - r * std::sin(phi[0]),
            rd * std::sin(phi[0]) + r * std::cos(phi[0])
    );
}

inline bool clover_inside(const mm::Vec2d &p) {
    return p.squaredNorm() <= mm::ipow<2>(clover_polar(std::atan2(p[1], p[0])));
}

/*
 * Density ranges from 0 to 1. Use dx + (Dx-dx)*clover_density(p) to vary density from dx to Dx.
 */
inline double clover_density(const mm::Vec2d &p) {
    return mm::ipow<2>(std::cos(3 * std::atan2(p[1], p[0]))) * std::tanh(p.norm());
}


class CloverShape : public mm::DomainShape<mm::Vec2d> {
public:
    typedef mm::Vec2d vector_t;
    typedef double scalar_t;

    bool contains(const vector_t &point) const override { return clover_inside(point); }

    std::pair <vector_t, vector_t> bbox() const override {
        vector_t lo = {-2.5, 2}, hi = {2.5, 3};
        return {lo, hi};
    }

    CloverShape *clone() const override { return new CloverShape(); }

    std::ostream &print(std::ostream &os) const override { return os << "CloverShape"; }

    mm::DomainDiscretization <vector_t>
    discretizeBoundaryWithDensity(const std::function<scalar_t(vector_t)> &dr, int type) const override {
        auto d = mm::DomainDiscretization<vector_t>(*this);
        return d;
    }
};

#endif //MEDUSA_EXTRAS_CLOVER_SHAPE_2D_H
