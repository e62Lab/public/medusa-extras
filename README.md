# Medusa

Coordinate Free Meshless Method implementation. See http://e6.ijs.si/medusa for more details.

# Medusa extras

During the development of our _Medusa_ library, some modules have been developed, but never
published to the [main repository](https://gitlab.com/e62Lab/medusa). Sometimes the modules were not
reliable, but in most cases they were developed specifically for a particular project. However, they
are not completely useless, so this repository has been created.

## Files

The following extras include but are not limited to:

- Custom domain shapes in the [DomainShapes](/DomainShapes/) folder.
- Exporting data helpers in the [ExportData](/ExportData/) folder.

## Authors

[E62Lab team](https://e6.ijs.si/ParallelAndDistributedSystems/)
