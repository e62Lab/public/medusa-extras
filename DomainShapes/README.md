# Domain shapes

Medusa supports custom domain shapes. However, sometimes these shapes are not the most trivial thing
to write, so we decided to gather some examples here. You may adapt them to your needs or simply use
them as they are.

## Usage

For usage please
check [official documentation](https://e6.ijs.si/medusa/docs/html/classmm_1_1DomainShape.html).
