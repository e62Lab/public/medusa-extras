#ifndef DISCRETIZATION_HELPER_TWO_HPP
#define DISCRETIZATION_HELPER_TWO_HPP

#include <medusa/Medusa_fwd.hpp>

using namespace mm;

template <typename Vec2d>
DomainDiscretization<Vec2d> build_2d_domain(const XML& conf, double dx) {
    // Points to be used in NURBS creation to create duck.
    Range<Vec2d> pts{{210, -132.5},  {205, -115},    {125, -35}, {85, -61},  {85, -61},
                     {85, -61},      {80, -65},      {75, -58},  {75, -58},  {65, 45},
                     {25, 25},       {-15, -16},     {-15, -16}, {-15, -16}, {-35, -28},
                     {-40, -32.375}, {-40, -32.375}, {-43, -40}, {-27, -40}, {-5, -40},
                     {-5, -40},      {50, -38},      {35, -65},  {15, -75},  {15, -75},
                     {-15, -95},     {-20, -140},    {45, -146}, {45, -146}, {95, -150},
                     {215, -150},    {210, -132.5}};
    double rescale = 1;
    Vec2d minimum = {0, 0};
    for (auto p : pts) {
        rescale = std::max(rescale, p.cwiseAbs().maxCoeff());
        minimum = minimum.cwiseMin(p);
    }

    // Calculate NURBS patches' control points, weights, knot vector and order.
    int p = 3;
    Range<double> weights{1, 1, 1, 1};
    Range<double> knot_vector{0, 0, 0, 0, 1, 1, 1, 1};
    Range<NURBSPatch<Vec2d, Vec1d>> patches;

    for (int i = 0; i < 8; i++) {
        Range<Vec2d> control_points;
        for (int j = 0; j < 4; j++) {
            control_points.push_back((pts[4 * i + j] - minimum) / rescale);
        }

        NURBSPatch<Vec2d, Vec1d> patch(control_points, weights, {knot_vector}, {p});
        patches.push_back(patch);
    }

    // Create NURBS shape from a range of patches.
    NURBSShape<Vec2d, Vec1d> shape(patches);
    DomainDiscretization<Vec2d> duck_domain = shape.discretizeBoundaryWithStep(1.0 / 120.0);

    
    GeneralFill<Vec2d> gf;
    gf.seed(seed);
    domain.fill(gf, dx);

    return domain;
}

#endif /* DISCRETIZATION_HELPER_TWO_HPP */
