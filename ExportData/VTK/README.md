# Export data to VTK

Data is exported to `*.vtk` file and can be opened by [Paraview](https://www.paraview.org).

## Usage

```c++
// Save results to VTK.
cout << "Saving to VTK." << endl;
std::stringstream fname;
fname << conf.get<string>("meta.out_vtk");

vtk::outputVTK vtk(d, "Phase change simulation", fname.str().c_str(), time_step, "../data");
vtk.addScalar(u, "temperature");
vtk.addScalar(d.types(), "type");
vtk.close();
```
