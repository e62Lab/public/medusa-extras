#!/bin/bash
XDMFFOLDER=$PWD
SCRFOLDER=$XDMFFOLDER/..
H5FOLDER=$XDMFFOLDER/../h5s
cd $H5FOLDER

for i in *.h5; do
    filename="${i%.*}"

    rm -f $XDMFFOLDER/$filename*

    python3 $SCRFOLDER/getxdmf.py $i $XDMFFOLDER/$filename
done
