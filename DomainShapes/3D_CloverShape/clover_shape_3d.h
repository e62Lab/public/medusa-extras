//
// Created by mitja on 10/01/2022.
//

#ifndef MEDUSA_EXTRAS_CLOVER_SHAPE_3D_H
#define MEDUSA_EXTRAS_CLOVER_SHAPE_3D_H

#include <medusa/Medusa.hpp>
#include <cmath>

inline double clover3_polar(double phi, double th) {
    return 1.5 - mm::ipow<3>(std::cos(3 * (phi - mm::PI / 6))) * mm::ipow<2>(th) * mm::ipow<2>(mm::PI - th) / 8;
}

inline bool clover3_inside(const mm::Vec3d &p) {
    double r = p.norm();
    double f = std::atan2(p[1], p[0]);
    double th = std::acos(p[2] / r);
    return r < clover3_polar(f, th);
}

/*
 * Density ranges from 0 to 1. Use dx + (Dx-dx)*clover_density(p) to vary density from dx to Dx.
 */
inline double clover3_density(const mm::Vec3d &p) {
    return mm::ipow<2>(std::cos(3 * std::atan2(p[1], p[0]) + mm::PI / 3)) * std::tanh(p.norm()) * (2 - p[2]);
}


class Clover3Shape : public mm::DomainShape<mm::Vec3d> {
public:
    typedef mm::Vec3d vector_t;
    typedef double scalar_t;

    bool contains(const vector_t &point) const override { return clover3_inside(point); }

    std::pair <vector_t, vector_t> bbox() const override {
        vector_t lo = {-2.5, 2, -1}, hi = {2.5, 3, 1};
        return {lo, hi};
    }

    Clover3Shape *clone() const override { return new Clover3Shape(); }

    std::ostream &print(std::ostream &os) const override { return os << "Clover3Shape"; }

    mm::DomainDiscretization <vector_t>
    discretizeBoundaryWithDensity(const std::function<scalar_t(vector_t)> &dr, int type) const override {
        auto d = mm::DomainDiscretization<vector_t>(*this);
        return d;
    }
};

#endif //MEDUSA_EXTRAS_CLOVER_SHAPE_3D_H
