#ifndef CAD_FILES_INPUT_HPP
#define CAD_FILES_INPUT_HPP

#include <medusa/Medusa_fwd.hpp>
#include <IGESControl_Reader.hxx>
#include <Interface_Static.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include <BRepBuilderAPI_NurbsConvert.hxx>
#include <Geom_Surface.hxx>
#include <BRepLib_FindSurface.hxx>
#include <Geom_BSplineSurface.hxx>
#include <GeomConvert.hxx>
#include <STEPControl_Reader.hxx>
#include <TopoDS_Iterator.hxx>

namespace mm {

/**
 * Converts OpenCASCADE Geom_BSplineSurface type to Medusa NURBSPatch.
 * @param nurbs_surf NURBS surface in the OpenCASCADE format.
 * @return NURBSPatch representing the given NURBS surface.
*/
NURBSPatch<Vec3d, Vec2d> bsplinesurface_to_nurbspatch(Geom_BSplineSurface& nurbs_surf) {
    // Get order
    std::array<int, 2> p = {nurbs_surf.UDegree(), nurbs_surf.VDegree()};

    // Get knots
    TColStd_Array1OfReal u_knots_arr = nurbs_surf.UKnotSequence();
    Range<double> u_knots;
    u_knots.reserve(u_knots_arr.Size());
    for (const auto &w : u_knots_arr) {
        u_knots.push_back(w);
    }
    TColStd_Array1OfReal v_knots_arr = nurbs_surf.VKnotSequence();
    Range<double> v_knots;
    v_knots.reserve(v_knots_arr.Size());
    for (const auto &w : v_knots_arr) {
        v_knots.push_back(w);
    }
    std::array<Range<double>, 2> knots = {std::move(u_knots), std::move(v_knots)};

    // Get weights
    int u_size = nurbs_surf.NbUPoles(), v_size = nurbs_surf.NbVPoles();
    TColStd_Array2OfReal arr_weights(0, u_size - 1, 0, v_size - 1);
    nurbs_surf.Weights(arr_weights);

    Range<Range<double>> weights;
    weights.resize(u_size);
    for (int u = 0; u < u_size; u++) {
        weights[u].resize(v_size);
        for (int v = 0; v < v_size; v++) {
            weights[u][v] = arr_weights.Value(u, v);
        }
    }

    // Get control points
    TColgp_Array2OfPnt arr_cp(0, u_size - 1, 0, v_size - 1);
    nurbs_surf.Poles(arr_cp);

    Range<Range<Vec3d>> control_points;
    control_points.resize(u_size);
    for (int u = 0; u < u_size; u++) {
        control_points[u].resize(v_size);
        for (int v = 0; v < v_size; v++) {
            gp_Pnt point = arr_cp.Value(u, v);
            control_points[u][v] = {point.X(), point.Y(), point.Z()};
        }
    }

    return NURBSPatch<Vec3d, Vec2d>(control_points, weights, knots, p);
}

/**
 * Translate IGES file into a NURBSShape.
 * @param file_name Path to the file.
 * @param force_c Force continuity: 0 - do nothing, 1 - force C1, 2 - force C2.
 * @param skip If true, skip all NURBS shapes of the first order (typically found if the file contains something in addition to NURBS surfaces).
 * @return NURBSShape representing the contents of the given file.
*/
NURBSShape<Vec3d, Vec2d> translate_IGES(const std::string &file_name, const int force_c = 0, const bool skip = false) {
    Interface_Static::SetIVal ("read.iges.bspline.continuity", force_c); // 0 - do nothing, 1 - force C1, 2 - force C2
    IGESControl_Reader reader;
    reader.ReadFile(file_name.c_str());

    Handle(TColStd_HSequenceOfTransient) nurbs_surface_entities = reader.GiveList("iges-type(128)");  // Get only nurbs surfaces
    std::clog << "Number of found NURBS entities: " << nurbs_surface_entities->Size() << std::endl;
    Standard_Integer nbtrans = reader.TransferList(nurbs_surface_entities);
    std::clog << "Number of translated entities: " << nbtrans << std::endl;

    // Translate to medusa nurbs patches
    Range<NURBSPatch<Vec3d, Vec2d>> patches;
    patches.reserve(reader.NbShapes());
    for (int i = 1; i <= reader.NbShapes(); i++) {
        // Convert shape into nurbs surface object
        TopoDS_Shape shape = reader.Shape(i);
        TopoDS_Face face = TopoDS::Face(shape);
        BRepBuilderAPI_NurbsConvert nurbs_convert_api(face);
        Handle(Geom_Surface) geom_surf = BRepLib_FindSurface(nurbs_convert_api.Shape()).Surface();
        Handle(Geom_BSplineSurface) nurbs_surf = GeomConvert::SurfaceToBSplineSurface(geom_surf);

        NURBSPatch<Vec3d, Vec2d> patch = bsplinesurface_to_nurbspatch(*nurbs_surf);
        if (skip && nurbs_surf->UDegree() == 1 && nurbs_surf->VDegree() == 1) {
            continue;
        }

        // Create patch
        patches.emplace_back(std::move(patch));
    }

    NURBSShape<Vec3d, Vec2d> sh(std::move(patches));
    return std::move(sh);
}

/**
 * Translate STEP file into a NURBSShape.
 * @param file_name Path to the file.
 * @param skip If true, skip all NURBS shapes of the first order (typically found if the file contains something in addition to NURBS surfaces).
 * @return NURBSShape representing the contents of the given file.
*/
NURBSShape<Vec3d, Vec2d> translate_STEP(const std::string &file_name, const bool skip = false) {
    STEPControl_Reader reader;
    reader.ReadFile(file_name.c_str());
    reader.TransferRoots();


    TopoDS_Iterator iter = TopoDS_Iterator(reader.Shape(1));
    TopoDS_Shape parent_shape = iter.Value();
    int number_of_entities = parent_shape.NbChildren();
    while (number_of_entities == 1) {
        iter = TopoDS_Iterator(parent_shape);
        parent_shape = iter.Value();
        number_of_entities = parent_shape.NbChildren();
    }
    std::clog << "Number of translated entities: " << number_of_entities << std::endl;

    iter = TopoDS_Iterator(parent_shape);

    Range<NURBSPatch<Vec3d, Vec2d>> patches;
    patches.reserve(number_of_entities);
    for (int i = 1; i <= number_of_entities; i++, iter.Next()) {
        // Convert shape into nurbs surface object
        TopoDS_Shape shape = iter.Value();
        TopoDS_Face face = TopoDS::Face(shape);
        BRepBuilderAPI_NurbsConvert nurbs_convert_api(face);
        Handle(Geom_Surface) geom_surf = BRepLib_FindSurface(nurbs_convert_api.Shape()).Surface();
        Handle(Geom_BSplineSurface) nurbs_surf = GeomConvert::SurfaceToBSplineSurface(geom_surf);

        NURBSPatch<Vec3d, Vec2d> patch = bsplinesurface_to_nurbspatch(*nurbs_surf);

        if (skip && nurbs_surf->UDegree() == 1 && nurbs_surf->VDegree() == 1) {
            continue;
        }

        // Create patch
        patches.emplace_back(std::move(patch));
    }

    NURBSShape<Vec3d, Vec2d> sh(std::move(patches));
    return std::move(sh);
}

}  // namespace mm

#endif // BEZIER_SUBDIVISION_HPP
